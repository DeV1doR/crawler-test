# -*- coding: utf-8 -*-
import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Compose, Join


class FleurItem(scrapy.Item):

    name = scrapy.Field()
    price = scrapy.Field()
    min_delivery_time = scrapy.Field()
    category = scrapy.Field()
    sku = scrapy.Field()
    is_avalilable = scrapy.Field()
    _pag_breadcrumbs = scrapy.Field()
    _prod_url = scrapy.Field()


class FleurItemLoader(ItemLoader):

    default_item_class = FleurItem
    is_avalilable_in = Compose(
        lambda v: u"Да" if "in-stock" in v[0] else u"Нет")
    category_out = Join(' > ')
    price_in = Compose(lambda arr: '0' if not arr else ''.join(arr))

    def load_item(self):
        if (getattr(self, 'is_actual', False)
           and self._values.get('price', ['0']) == ['0']):
            return
        return super(FleurItemLoader, self).load_item()
