import os
from scrapy.exporters import CsvItemExporter


class CategoryPipeline(object):

    def process_item(self, item, spider):
        try:
            category_name = os.path.join(
                *self.category2catalog(item, spider))
            file_name = spider.settings['CATEGORY_EXPORT_NAME']
            feed_folder = spider.settings['FEED_FOLDER']
            directory = os.path.join(feed_folder, spider.name, category_name)
            file_name = os.path.join(directory, file_name)
        except (KeyError, IndexError):
            return

        if not os.path.exists(directory):
            os.makedirs(directory)

        headers_not_written = True if not os.path.isfile(file_name) else False

        with open(file_name, 'a+') as cat_file:
            exporter = CsvItemExporter(cat_file)
            exporter._headers_not_written = headers_not_written
            exporter.fields_to_export = spider.settings['FEED_EXPORT_FIELDS']
            exporter.export_item(item)

        return item

    def category2catalog(self, item, spider):
        base_catalog_names = spider.dict_start_urls.keys()
        try:
            splited_cat = item['category'].split(' > ')[:-1]
        except IndexError:
            raise
        if splited_cat[1] not in base_catalog_names:
            # subcategory breadcrumb + catalog name
            splited_cat = item['_pag_breadcrumbs'] + splited_cat[-1:]
        return splited_cat[1:]
